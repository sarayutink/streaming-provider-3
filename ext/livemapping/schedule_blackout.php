<?php
class Worker {
	public function insertIntoRedis () {
		$mysql_array = $this->listTodaySchedule();
		if ($mysql_array !== false)
		{
			var_dump($mysql_array);
			$now = strtotime("now");
			foreach($mysql_array as $row) {
				$starttime = strtotime(date("Y-m-d", $now) ." ". $row['starttime']);
				$endtime = strtotime(date("Y-m-d", $now) ." ". $row['endtime']);
				$endtime = ($endtime > $starttime) ? $endtime : strtotime(date("Y-m-d", strtotime("+1 day")) ." ". $row['endtime']);
				$expire = strtotime(date("Y-m-d", $now) ." ". $row['endtime']) - $now;
				$key = $endtime .";". $starttime .";". $row['appid'] .";". $row['channel'] .";sch". $row['id'];
				$this->setRedis($key, 1, $expire);
				#file_put_contents("/scripts/blackout_service/logs/redis_blackout_schedule.log", date('Y-m-d H:i:s')." [SET] {$key}\n", FILE_APPEND);
			}
		}
	}
	
	public function listTodaySchedule () {
		$conn = Mysqlscope::getMysqlConnection();
		$sql = "SELECT s.id, a.appid, cs.channel, s.starttime, s.endtime FROM tbl_schedule s LEFT JOIN tbl_appid a ON a.id = s.appid LEFT JOIN tbl_days_schedule ds ON s.id = ds.id_schedule LEFT JOIN tbl_channels_schedule cs ON s.id = cs.id_schedule WHERE s.flagstatus = 1 AND s.flagdelete = 1 AND ds.day_num = ". date("N", strtotime("now")) ." AND s.endtime > '". date("H:i", strtotime("now")) ."';";
		echo $sql;
		// $sql = "SELECT s.id, a.appid, cs.channel, s.starttime, s.endtime FROM tbl_schedule s LEFT JOIN tbl_appid a ON a.id = s.appid LEFT JOIN tbl_days_schedule ds ON s.id = ds.id_schedule LEFT JOIN tbl_channels_schedule cs ON s.id = cs.id_schedule WHERE s.flagstatus = 1 AND s.flagdelete = 1 AND ds.day_num = ". 4 .";";
		$result = mysqli_query($conn,$sql) or die(false);
		$return = mysqli_fetch_all($result, MYSQLI_ASSOC);
		mysqli_free_result($result);
		Mysqlscope::releaseMysqlConnection($conn);
		
		return $return;
	}
	
	private function setRedis ($key, $value, $expire) {
		$redis = new Redis();
		$hosts = array("10.18.19.96", "10.18.19.98", "10.18.19.126");
		$port = "6380";
		$db = "3";
			// var_dump($redis);
		
		$return = 0;
		foreach ($hosts as $host) {
			try {
				$redis->connect($host, $port);
				$redis->select($db);
				$redis->set($key, $value);
				$redis->expire($key, $expire);
				$redis->close();
				$return += 0;
			} 
			catch (Exception $e) {
				$return += 1;
			}
			// echo __DIR__."/logs/blackout_schedule_". date("Ymd") .".log";
			file_put_contents(__DIR__."/logs/blackout_schedule_". date("Ymd") .".log", date("H:i:s") ." - [SETEX] $key $expire INTO $host:$port<$db>\n", FILE_APPEND);
		}
		unset($redis);
		return $return == 0;
	}
	
	public function delRedis () {
		$redis = new Redis();
		$hosts = array("10.18.19.96", "10.18.19.98", "10.18.19.126");
		$port = "6380";
		$db = "3";
		
		$return = 0;
		foreach ($hosts as $host) {
			try {
				$redis->connect($host, $port);
				$redis->select($db);
				$keys = $redis->keys("*;". $this->id);
				$redis->close();
				
				foreach ($keys as $key) {
					$redis->connect($host, $port);
					$redis->select($db);
					$redis->del($key);
					$redis->close();
				}
				$return += 0;
			} 
			catch (Exception $e) {
				$return += 1;
			}
			file_put_contents(__DIR__."/logs/blackout_schedule_". date("Ymd") .".log", date("H:i:s") ." - [DEL] $key INTO $host:$port<$db>\n", FILE_APPEND);
		}
		unset($redis);
		return $return == 0;
	}
}

class Mysqlscope {
	const HOST = '10.18.19.105';
	const USERNAME = 'root';
	const PASSWORD = 'ReleasePassword#1234';
	const DBNAME = 'blackout_service';
	
	public static function getMysqlConnection () {
		$conn = mysqli_connect(self::HOST, self::USERNAME, self::PASSWORD, self::DBNAME);
		return ($conn ? $conn : false);
	}
	
	public static function releaseMysqlConnection ($conn) {
		mysqli_close($conn);
	}
}

$woker = new Worker();
// var_dump($woker->insertIntoRedis());
$woker->insertIntoRedis();