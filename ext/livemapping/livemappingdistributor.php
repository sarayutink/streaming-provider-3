<?
require_once dirname(dirname(__DIR__)) .'/spmiddleware/libraries/Mwredis.php';

$channel_file = __DIR__ .'/channels.list';
$channels = explode(";", trim(file_get_contents($channel_file)));
foreach ($channels as $channel) {
	writeXML($channel);
	// break;
}

function writeXML ($channel) {
	$redis = new Mwredis();
	$lmap = $redis->getRedis($channel, 1);
	file_put_contents(__DIR__ ."/channelconfig/{$channel}.xml", $lmap);
}