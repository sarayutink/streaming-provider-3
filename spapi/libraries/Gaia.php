<?php
require_once __DIR__.'/vendor/autoload.php';

use \GeoIp2\Database\Reader;

class Gaia {
    const MMDB = '/usr/local/share/GeoIP/GeoIP2-City.mmdb';

    public static function getCountry ($ip) {
        $reader = new Reader(self::MMDB);
        $record = $reader->city($ip);
        return $record->country->isoCode;
    }
}