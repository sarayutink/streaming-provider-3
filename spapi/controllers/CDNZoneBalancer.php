<?
class CDNZoneBalancer {
    private $redis, $host, $db, $port;

    private $subnet;
    private $find_array, $flip_find_array, $profiles, $tointersect, $needle;
	
	public function findCDNByIP () {
        $this->find_array = array(
            "uid" => $GLOBALS['json_object']['uid'],
            "sessionid" => $GLOBALS['json_object']['sessionid'],
            "appid" => $GLOBALS['json_object']['appid'],
            "channelid" => $GLOBALS['json_object']['channelid'],
            "streamname" => $GLOBALS['json_object']['streamname'],
            "streamlvl" => $GLOBALS['json_object']['streamlvl'],
            "type" => $GLOBALS['json_object']['type'],
            "visitor" => $GLOBALS['json_object']['visitor'],
            "drm" => $GLOBALS['json_object']['drm']);
        $this->redis = new Redis();
        $this->host = '10.18.12.101'; $this->port = LBRDPORT; $this->db = array("network" => 5, "profile" => 6, "weight" => 7, "pool" => 8, "overflow" => 9);
        
		try {
			$this->redis->connect($this->host, $this->port);

            ## map from ip
            $separate_ip_arr = array(); $key = "";
            if (strpos($GLOBALS['json_object']['csip'], ".")) {
                $separate_ip_arr = explode(".", $GLOBALS['json_object']['csip']);
                $key = $separate_ip_arr[0].".".$separate_ip_arr[1].".*";
            } else {
                $separate_ip_arr = explode(":", $GLOBALS['json_object']['csip']);
                $key = $separate_ip_arr[0].":".$separate_ip_arr[1]."*";
            }
			$this->redis->select($this->db['network']);
			$subnets = $this->redis->keys($key);
			if (count($subnets) > 0) array_walk($subnets, array($this,'search_subnet'));
            $this->subnet = is_null($this->subnet) ? "other" : $this->subnet;
			$this->redis->select($this->db['network']);
            $node_code = $this->redis->get($this->subnet);
            // echo json_encode(array("node_code" => $node_code));
			$this->find_array['node_code'] = $node_code;
            $this->flip_find_array = array_flip($this->find_array);
            array_walk($this->find_array, array($this,'map_from_profiles'));
            // echo json_encode(array("find_array" => $this->find_array, "flip_find_array" => $this->flip_find_array, "profiles" => $this->profiles));
            array_map(array($this,'getProfileScore'), array_keys($this->profiles), array_values($this->profiles));
            // echo json_encode(array("find_array" => $this->find_array, "flip_find_array" => $this->flip_find_array, "profiles" => $this->profiles));
            // echo json_encode(array("profiles" => $this->profiles));
            
            do {
                asort($this->profiles);
                $this->needle = end(array_flip($this->profiles));
                $cdn = $this->getCDN($this->needle);
                // echo json_encode(array("profiles" => $this->profiles, "needle" => $this->needle, "cdn" => $cdn));
                unset($this->profiles);

                $this->find_array['profile_id'] = $this->needle;
                $this->flip_find_array = array_flip($this->find_array);
                array_walk($this->find_array, array($this,'map_from_overflows'));
                array_map(array($this,'getProfileScore'), array_keys($this->profiles), array_values($this->profiles));
                // echo json_encode(array("find_array" => $this->find_array, "flip_find_array" => $this->flip_find_array, "profiles" => $this->profiles));
            } while (!empty($this->profiles) && $cdn['result_code'] != 200);
            // $cdn = array("result_code" => 421, "result" => "GROUP_NOT_FOUND", "QUERY" => serialize($this->find_array));
            
			$this->redis->close();
		} catch (Exception $e) {
			file_put_contents(LOGDIR ."/NewCDNZoneBalancer.err", date("Y/m/d_H:i:s") ."  ". $this->host.":".$this->port ."  ". $e->getMessage()."\n", FILE_APPEND);
            return array("result_code" => 428, "result" => "REDIS_CDN_GROUP_WENT_AWAY");
		}

        // return array("result_code" => 200, "result" => "true");
        return $cdn;
    }

    private function getCDN ($profile_id) {
        try {
            $this->redis->select($this->db['pool']);
            $cdnsdesc = $this->redis->get($profile_id);
            $cdns_arr = json_decode($cdnsdesc, true);
            if (count($cdns_arr) != 0) {
                switch (count($cdns_arr)) {
                    case 0:
                        $return = array("result_code" => 422, "result" => "[$profile_id]_IS_EMPTY");
                        break;
                    case 1 :
                        $return = array("result_code" => 200, "result" => $cdns_arr[0]['host']);
                        break;
                    case 2 :
                        $return = array("result_code" => 200, "result" => $cdns_arr[array_rand($cdns_arr)]['host']);
                        break;
                    default :
                        $samples = array_rand($cdns_arr, 2);
                        $result = $cdns_arr[$samples[0]]['ccu_available'] > $cdns_arr[$samples[1]]['ccu_available'] ? $cdns_arr[$samples[0]]['host'] : $cdns_arr[$samples[1]]['host'];
                        $return = array("result_code" => 200, "result" => $result);
                        break;
                }
                return $return;
            }
            else return array("result_code" => 422, "result" => "[$profile_id]_IS_EMPTY");
        }
        catch (Exception $e) {
			file_put_contents(LOGDIR ."/NewCDNZoneBalancer.err", date("Y/m/d_H:i:s") ."  ". $this->host.":".$this->port ."  ". $e->getMessage()."\n", FILE_APPEND);
            return array("result_code" => 428, "result" => "REDIS_CDN_GROUP_WENT_AWAY");
        }
    }
    
    private function search_subnet ($item) {
        $this->check_acl($GLOBALS['json_object']['csip'], $item);
    }

	private function check_acl($ip, $cidr) {
		$ipb = inet_pton($ip);
		$iplen = strlen($ipb);
		if (strlen($ipb) < 4) exit(0);
		
		$ar = explode('/',$cidr);
		$ip1 = $ar[0];
		$ip1b = inet_pton($ip1);
		$ip1len = strlen($ip1b);
		if ($ip1len == $iplen) {
			$isincidr = true;
			if (count($ar)>1) $bits=(int)($ar[1]);
			else $bits = $iplen * 8;
			
			for ($c=0; $bits>0; $c++) {
				$bytemask = ($bits < 8) ? 0xff ^ ((1 << (8-$bits))-1) : 0xff;
				if (((ord($ipb[$c]) ^ ord($ip1b[$c])) & $bytemask) != 0) {
					$isincidr = false;
					break;
				}
				$bits-=8;
			}
			if ($isincidr) {
				$ex_ar = explode("/", $this->subnet);
				if ($ar[1] > @$ex_ar[1]) $this->subnet = $cidr;
			}
		}
    }

    private function map_from_profiles ($key) {
        $this->map_from_params($key, 'profile');
    }

    private function map_from_overflows ($key) {
        $this->map_from_params($key, 'overflow');
    }

    private function map_from_params ($key, $function) {
        $bitwise = array(
            "profile_id"        => 0b10000000000000,
            "uid"               => 0b00001000000000,
            "sessionid"         => 0b00000100000000,
            "appid"             => 0b01000000000000,
            "channelid"         => 0b00000010000000,
            "streamname"        => 0b00000000100000,
            "type"              => 0b00100000000000,
            "visitor"           => 0b00010000000000,
            "drm"               => 0b00000000010000,
            "node_code"         => 0b00000000000100,
            "streamlvl"         => 0b00000001000000,
            "OPERATOR_BIT"      => 0b00000000001000,
            "LOGIN_BIT"         => 0b00000000000010,
            "WHITE_LIST_BIT"    => 0b00000000000001
        );

		$this->redis->select($this->db[$function]);
        $result = $this->redis->sMembers($key);
        if ($result !== false && count($result) > 0) {
            foreach ($result as $profile) {
                // echo $key." => ".$profile." ".$bitwise[$this->flip_find_array[$key]]."\n";
                $this->profiles[$profile] += $bitwise[$this->flip_find_array[$key]];
            }
        }
    }

    private function getProfileScore ($key, $value) {
        $this->redis->select($this->db['weight']);
        $profile_score = $this->redis->get($key.@$this->needle);

        if ($profile_score != $value) {
            // echo "unset key=$key, value=$value, profile_score = $profile_score\n";
            unset($this->profiles[$key]);
        }
        // else echo "still key=$key, value=$value, profile_score = $profile_score\n";
    }

    private function defaultvalue ($paramname, $paramval) {
        $default_map = array("node_code" => "other");
        if (in_array($paramname, array_flip($default_map))) {
            if ($paramval === null) return array($default_map[$paramname]);
            elseif ($paramval === false) return array($default_map[$paramname]);
        }
        else return $paramval;
    }
}
?>