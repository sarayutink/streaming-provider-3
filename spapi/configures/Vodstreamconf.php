<?
class Vodstreamconf {
    
    public function overrideparams () {
        if ($GLOBALS['json_object']['drm'] == "wv1") $GLOBALS['json_object']['drm'] = "wv";
    }

    public function isCCOver () {
		require_once SRCDIR .'/configures/Businesconf.php';
		$bizconf = new Businesconf();
		if (@$GLOBALS['json_object']['ccucheck'] === true) return $bizconf->isCCuOverGroup($GLOBALS['json_object']['uid'], $GLOBALS['json_object']['sessionid'], $GLOBALS['json_object']['streamname']);
		else return false;
  	}
	
	public function isBlacklist () {
        return !is_bool($GLOBALS['rd_3']->getRedis($GLOBALS['json_object']['uid'], 7));
    }
	
    
	public function generatePlaylist () {
		$streamname = $GLOBALS['json_object']['visitor'] == "stb" ? "_stb_auto.smil" : "_m_auto.smil";;
		switch($GLOBALS['json_object']['type']) {
			case "tvod":
				$streamname = str_pad($GLOBALS['json_object']['streamname'], 12, "0", STR_PAD_LEFT) . $streamname;
			break;
			case "svod":
				$streamname = str_pad($GLOBALS['json_object']['streamname'], 12, "0", STR_PAD_LEFT) . $streamname;
			break;
			case "clip":
				$streamname = str_replace(".mp4", "", $GLOBALS['json_object']['streamname']) . $streamname;
			break;
		}
		$GLOBALS['signature'] = $this->signQueryString($streamname);
        
        return $GLOBALS['json_object']['type']."_".$GLOBALS['json_object']['drm']."/".$streamname;
    }
	
	public function getdrmqstring () {
        $drm = $GLOBALS['json_object']['drm'];
        if ($drm == "wv") $drm = "wv3";
		$querystring = "appid={$GLOBALS['json_object']['appid']}&type={$GLOBALS['json_object']['type']}&visitor={$GLOBALS['json_object']['visitor']}&drm={$drm}&uid={$GLOBALS['json_object']['uid']}&did=". str_replace(array('+', '/', '='), array('-', '_' , ''), base64_encode($GLOBALS['json_object']['sessionid']));
		
		return $querystring;
	}
	
	public function getrsaqstring ($bypass = false) {
        $drm = $GLOBALS['json_object']['drm'];
        if ($drm == "wv") $drm = "wv3";
        $streamname = current(explode("_", str_replace(".mp4", "", $GLOBALS['json_object']['streamname'])));
        $mpass = $bypass === false ? Opensslcryption::encrypt(time() ."|". $GLOBALS['json_object']['sessionid'] ."|". $GLOBALS['json_object']['appid'] ."|". $GLOBALS['json_object']['csip'] ."|". $streamname ."|". $GLOBALS['json_object']['uid'] ."|". $type) : Opensslcryption::encryptbypass();

		@$querystring .= "appid={$GLOBALS['json_object']['appid']}&type={$GLOBALS['json_object']['type']}&visitor={$GLOBALS['json_object']['visitor']}&drm={$drm}&uid={$GLOBALS['json_object']['uid']}&did=". str_replace(array('+', '/', '='), array('-', '_' , ''), base64_encode($GLOBALS['json_object']['sessionid'])) ."&mpass={$mpass}";
		
		return $querystring;
	}
	
	public function signQueryString ($smil) {
		$secret = "ǝɯƖ┴ǝɔ@dSln0SʎʇƖl@ǝɹɹǝʍ0ԀpuƖW";
		$sid = bin2hex(openssl_random_pseudo_bytes(5));
		$rt = "".time();
		
		$payload = array($sid => array(
			$GLOBALS['json_object']['appid'], 
			$smil,
			$GLOBALS['json_object']['type'],
			$GLOBALS['json_object']['visitor'],
			"".$GLOBALS['json_object']['uid'],
			str_replace(array('+', '/', '='), array('-', '_' , ''), base64_encode($GLOBALS['json_object']['sessionid'])),
			$rt
		));
		
		$payloadKey = hash_hmac("sha256", $secret, $sid);
		
		$token = str_replace(array("+", "/", "="), array("-", "_", ""), base64_encode(hash_hmac("sha256", json_encode($payload), $payloadKey, true)));
		
		return "&sid={$sid}&rt={$rt}&tk={$token}";
	}
}
