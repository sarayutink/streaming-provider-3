<?
require_once __DIR__."/spapi/config.req.php";
// define("SPRDHOST", "sprdhost");
// define("SPRDPORT", 6380);
// define("SPRDDB", serialize(array(1, 3, 4, 5, 6, 7)));
// define("BZRDHOST", "sprdhost");
// define("BZRDPORT", 6383);
// define("BZRDDB", serialize(array(3, 4)));
// define("LBRDHOST", "lbrdhost");
// define("LBRDPORT", 6383);
// define("LBRDDB", serialize(array(0, 499)));

class Hestia {
    private $redis;

    public function __Construct () {
        $this->redis = new Redis();
    }

    public function livemaphealth () {
        $redis_score = 0;
        # livemapping redis
        $focus = array(1, 3, 6);
        try {
            $this->redis->connect(SPRDHOST, SPRDPORT);
            $keyspaces = $this->redis->info("keyspace");
            foreach ($focus as $db) if (!isset($keyspaces["db".$db])) $redis_score += $db;
            $this->redis->close();
        }
        catch (Expection $e) {
            $redis_score += 1;
            $error_message = $this->getMessage(__FUNCTION__, $e);
            file_put_contents(LOGDIR."/healthcheck_error_". date("Ymd_H") .".log", $error_message, FILE_APPEND);
        }
        return $redis_score;
    }

    public function bizconfhealth () {
        return true;
    }

    public function lbhealth () {
        $redis_score = 0;
        # livemapping redis
        $focus = array(8);
        try {
            $this->redis->connect(SPRDHOST, LBRDPORT);
            $keyspaces = $this->redis->info("keyspace");
            foreach ($focus as $db) if (!isset($keyspaces["db".$db])) $redis_score += $db;
            $this->redis->close();
        }
        catch (Expection $e) {
            $redis_score += 1;
            $error_message = $this->getMessage(__FUNCTION__, $e);
            file_put_contents(LOGDIR."/healthcheck_error_". date("Ymd_H") .".log", $error_message, FILE_APPEND);
        }
        return $redis_score;
    }

    public function apihealth () {
        return true;
    }

    public function connectionhealth () {
        $redis_score = 0;
        # livemapping redis
        try {
            $this->redis->connect(SPRDHOST, SPRDPORT);
            $resp = $this->redis->ping();
            $this->redis->close();
        }
        catch (Expection $e) {
            $redis_score += 1;
            $error_message = $this->getMessage(__FUNCTION__, $e);
            file_put_contents(LOGDIR."/healthcheck_error_". date("Ymd_H") .".log", implode("  ", $log_arr), FILE_APPEND);
        }
        # cdnpool redis
        try {
            $this->redis->connect(SPRDHOST, LBRDPORT);
            $resp = $this->redis->ping();
            $this->redis->close();
        }
        catch (Expection $e) {
            $redis_score += 10;
            $error_message = $this->getMessage(__FUNCTION__, $e);
            file_put_contents(LOGDIR."/healthcheck_error_". date("Ymd_H") .".log", implode("  ", $log_arr), FILE_APPEND);
        }
        return $redis_score;
    }

    private function getMessage ($functionname, $exception) {
        $error_message = implode("  ", array(date("Y/m/d H:i:s"), "[".strtoupper($functionname)."]", $exception->getMessage(), "\n"));
    }
}
var_dump((SPRDHOST));
var_dump(dns_get_record(SPRDHOST));
$healthcheck = new Hestia();
$livemapping_score = $healthcheck->livemaphealth();
$cdnprofile_score = $healthcheck->lbhealth();
$connection_score = $healthcheck->connectionhealth();
echo "Livemapping error code summary >> ". $livemapping_score ."\n";
echo "CDN profile error code summary >> ". $cdnprofile_score ."\n";
echo "Redis connection error code summary >> ". $connection_score ."\n";