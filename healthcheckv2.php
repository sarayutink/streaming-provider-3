<?php
function is_establish_redis ($host = '10.18.19.127', $port = '6380', $db = 1) {
	$redisexestarttime = microtime(true);
	$redis = new Redis();
	$redis->connect($host, $port);
	$redis->select($db);
	$return = $redis->keys("*");
	$redis->close();
	$redisexeendtime = microtime(true);
	$redisexetime = $redisexeendtime - $redisexestarttime;
	return array("value" => !empty($return), "exetime" => $redisexetime);
}

function is_establish_balancer ($url = "http://server_loadbalan:8080/lalaynya.php") {
	$lblexestarttime = microtime(true);
	$url .= "?group=11&uid=healthcheckv2";
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_USERAGENT, "Streaming Provider 3 ($url)");
	if(curl_errno($curl)) $ret = false;
	else $ret = true;
	curl_close($curl);
	$lblexeendtime = microtime(true);
	$lblexetime = $lblexeendtime - $lblexestarttime;
	return array("value" => $ret, "exetime" => $lblexetime);
}

function is_establish_service () {
	$ch_arr = array("021", "058", "064", "080", "082", "086", "107", "108", "109", "110", "111", "114", "127", "135", "139", "143", "148", "151", "152", "154", "159", "179", "207", "c03", "c05", "c07", "c09", "c11", "c12", "d03", "d05", "d11", "d13", "d23", "d33", "d43", "d48", "d54", "d56", "d62", "d76", "d78", "d81", "d83", "da0", "da1", "da6", "da7", "o009", "o012", "o013", "o014", "o015", "o016", "o017", "o018", "o019", "o020", "o021", "o022", "o023", "o024", "o025", "o026", "o027", "o028", "o029", "o030", "o031", "o032", "o033", "o039", "o042", "o043", "o044", "o045", "o053", "o055", "o057", "o058", "o059", "o060", "o062", "o063", "o065", "o066", "o068", "o069", "o070", "o071", "o064");
	$url = "http://providers3.ubc.co.th/streamingproviderv2";
	$body = array(
		"uid" => "healthcheckv2",
		"sessionid" => null,
		"appid" => "testv2",
		// "appid" => "trueidv2",
		"channelid" => $ch_arr[array_rand($ch_arr)],
		"langid" => "th",
		"streamlvl" => "auto",
		"type" => "live",
		"stime" => null,
		"duration" => null,
		"csip" => null,
		"geoblock" => false,
		"gps" => null,
		"agent" => "healthcheckv2",
		"visitor" => "mobile"
	);
	$exestarttime = microtime(true);
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($body));
	curl_setopt($curl, CURLOPT_USERAGENT, "Streaming Provider 3 ($url)");
	if(curl_errno($curl)) $ret = false;
	else {
		$ret = curl_exec($curl);
		$ret = json_decode($ret, true);
		$ret = $ret['result_code'];
	}
	curl_close($curl);
	$exeendtime = microtime(true);
	$exetime = $exeendtime - $exestarttime;
	return array("value" => $ret, "exetime" => $exetime);
}

if (@$_REQUEST['param'] == "redis") {
	$redis = is_establish_redis();
	echo number_format($redis["exetime"], 6);
}
else if (@$_REQUEST['param'] == "balancer") {
	$lbl = is_establish_balancer();
	echo number_format($lbl["exetime"], 6);
}
else if (@$_REQUEST['param'] == "service") {
	$sp3 = is_establish_service();
	echo number_format($sp3["exetime"], 6);
}
else {
	$redis = is_establish_redis();
	$lbl = is_establish_balancer();
	$sp3 = is_establish_service();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Health Check</title>
<style type="text/css">
html * {
  font-style: normal;
  font-family: arial, verdana, tahoma;
  font-size: 12px;
}

table {
  border-collapse: collapse;
}

th, td {
  padding: 2px 3px;
}

th {
  background-color: #000;
  color: #fff;
}

td {
  border-bottom: 1px solid #ccc;
}

tr.bg {
  background-color: #f0f0f0;
}
</style>
</head>
<body>
  <table width="100%">
    <tr>
      <td width="50%"><b>Service Name:</b> http://providers3.ubc.co.th</td>
      <td><b>Date/Time:</b> <?php echo date("Y-m-d H:i:s"); ?></td>
    </tr>
    <tr>
      <td><b>Module Name:</b> web</td>
      <td></td>
    </tr>
  </table>
  <p>redis, loadbalance</p>
  <table width="100%">
    <tr>
      <th>No.</th>
      <th>Description</th>
      <th>Type</th>
      <th>Param</th>
      <th>Status</th>
      <th>Value</th>
      <th>Solution</th>
    </tr>
      <tr class="bg">
      <td>1</td>
      <td>Connect to Redis, host: 10.18.19.127</td>
      <td>Redis</td>
      <td>redis</td>
      <td><?php echo ($redis["value"] ? "OK" : "ERROR"); ?></td>
      <td><?php echo number_format($redis["exetime"], 6); ?></td>
      <td></td>
    </tr>
      <tr class="bg">
      <td>3</td>
      <td>Call Balancer API - http://server_loadbalan:8080/lalaynya.php</td>
      <td>Balancer</td>
      <td>balancer</td>
      <td><?php echo ($lbl["value"] ? "OK" : "ERROR"); ?></td>
      <td><?php echo number_format((float)$lbl["exetime"], 6); ?></td>
      <td></td>
    </tr>
      <tr class="bg">
      <td>3</td>
      <td>Request /streamingprovidersv2 - http://provider3.ubc.co.th/streamingprovidersv2</td>
      <td>Streaming Provider</td>
      <td>service</td>
      <td><?php echo ($sp3["value"]); ?></td>
      <td><?php echo number_format((float)$sp3["exetime"], 6); ?></td>
      <td></td>
    </tr>
    </table>
  <?php if ($redis["value"] && $lbl["value"] && ($sp3["value"] == 420 || $sp3["value"] == 200)) echo "THIS_PAGE_IS_COMPLETELY_LOADED"; ?></body>
</html>
<?php } ?>
