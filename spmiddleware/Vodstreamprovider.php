<?
require_once 'Vodstreamprovider.req';

class Vodstreamprovider {
	public static function provide ($request, $response) {
		#$body = $request->getBody();
		$body = file_get_contents("php://input");
		$GLOBALS['ctrl_name'] = "Vodstreamprovider";
		$GLOBALS['lbl_name'] = "lalaynya";
		$GLOBALS['json_object'] = json_decode($body);
		unset($body);
		
		## validate post json
		if (Variable::validate()) {
			$apconf = ucfirst($GLOBALS['json_object']->appid)."config";
			if (file_exists($GLOBALS['src_dir'] ."/configures/vodstreamprovider/".$apconf.".php")) {
				require_once($GLOBALS['src_dir'] ."/configures/vodstreamprovider/".$apconf.".php");
				$GLOBALS['bizconf'] = new $apconf();
				## check ccu control
				// require_once($GLOBALS['src_dir'] ."/controllers/Ccucontrol.php");
				// if (Ccucontrol::check()) {
				if (true) {
					## get streaming server via load balancer
					require_once($GLOBALS['src_dir'] ."/controllers/Loadbalancecontrol.php");
					// $group_arr = array('sportclip_truetv', 'sportclip_trueidv2', 'skyclipv2');
					$group = $GLOBALS['bizconf']->getGroupId();
					$actionURL = $GLOBALS['bizconf']->getBalencer();
					$server = Loadbalancecontrol::findServer($actionURL, $group);
				// require_once($GLOBALS['src_dir'] ."/controllers/Roundbalancecontrol.php");
				// $in_group = $GLOBALS['bizconf']->getGroupId();
				// $server = Roundbalancecontrol::findServer($in_group);
					if (!is_int($server)) {
						// require_once($GLOBALS['src_dir'] ."/controllers/Streamcontrol.php");
						// $action = Streamcontrol::createStreamPath();
						// if(!is_null($action)) {
						// 	$return = array('result_code' => 200, 'result' => "http://".$server.$action);
						// 	Logger::writelog(array('result_code' => 200, 'result' => "http://".$server.$action));
						// }
						// else $return = array('result_code' => 430, 'result' => "Cannot find playlist.");
						require_once($GLOBALS['src_dir'] ."/controllers/Streamcontrol_dev.php");
						$action = Streamcontrol_dev::createStreamManifest();
						if (!is_null($action)) {
							switch (@$GLOBALS['json_object']->drm) {
								case "wv":
									$return = array('result_code' => 200, 'result_description' => "SUCCESS", 'streamurl' => "https://".$server.$action['manifest'], 'license' => $GLOBALS['bizconf']->bizconf->license->{$GLOBALS['json_object']->drm} ."?". $action['license']);
									Logger::writelog(array('result_code' => 200, 'result' => "https://".$server.$action['manifest'], 'license' => $action['license']));
								break;
								case "fp":
									$return = array('result_code' => 200, 'result_description' => "SUCCESS", 'streamurl' => "https://".$server.$action['manifest'], 'license' => $GLOBALS['bizconf']->bizconf->license->{$GLOBALS['json_object']->drm} ."?". $action['license']);
									Logger::writelog(array('result_code' => 200, 'result' => "https://".$server.$action['manifest'], 'license' => $action['license']));
								break;
								default:
									$return = array('result_code' => 200, 'result' => "https://".$server.$action['manifest'].$action['license']);
									Logger::writelog(array('result_code' => 200, 'result' => "https://".$server.$action['manifest'].$action['license']));
								break;
							}
						// 	$return = array('result_code' => 200, 'result_description' => "SUCCESS", 'streamurl' => "https://".$server.$action['manifest'], 'license' => $GLOBALS['bizconf']->bizconf->license->{$GLOBALS['json_object']->drm} ."?". $action['license']);
						// 	Logger::writelog(array('result_code' => 200, 'result' => "https://".$server.$action['manifest'], 'license' => $action['license']));
						}
						else $return = array('result_code' => 430, 'result_description' => "Cannot find playlist.");
					}
					else $return = array('result_code' => 420, 'result' => "Cannot find streaming server.");
				}
				else $return = array('result_code' => 410, 'result' => "Your concurrent reaches limit.");
			}
			else $return = array('result_code' => 600, 'result' => "Invalid request.");
		}
		else $return = array('result_code' => 600, 'result' => "Invalid request.");
		
		if ($GLOBALS['bizconf']->isBlacklist()) {
			$return["result_code"] = 210;
			$return["result"] = "https://thumbnail.stm.trueid.net/blacklist/playlist.m3u8";
			Logger::writelog($return);
		}
		if ($GLOBALS['bizconf']->isCCuOverGroup()) {
			$return["result_code"] = 220;
			$return["result"] = "https://thumbnail.stm.trueid.net/ccu_limitation/playlist.m3u8";
			Logger::writelog($return);
		}
		
		$return['version'] = "2.0";
		return $return;
    }
}