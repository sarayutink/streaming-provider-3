<?
require_once 'Streamingprovider.req';

class Streamingprovider {
	public static function provide ($request, $response) {
		#$body = $request->getBody();
		$body = file_get_contents("php://input");
		$GLOBALS['ctrl_name'] = "Streamingprovider";
		$GLOBALS['lbl_name'] = "lalaynya";
		$GLOBALS['json_object'] = json_decode($body);
		unset($body);
		
		## validate post json
		if (Variable::validate()) {
			$apconf = ucfirst($GLOBALS['json_object']->appid)."config";
			require_once($GLOBALS['src_dir'] ."/configures/streamingprovider/".$apconf.".php");
			$GLOBALS['bizconf'] = new $apconf();
			## check channel
			if ($GLOBALS['bizconf']->isValidChannelID()) {
				## check ccu control
				require_once($GLOBALS['src_dir'] ."/controllers/Ccucontrol.php");
				if (Ccucontrol::check() == 200) {
					## get streaming server via load balancer
					require_once($GLOBALS['src_dir'] ."/controllers/Loadbalancecontrol.php");
					$server = Loadbalancecontrol::findServer();
					// require_once($GLOBALS['src_dir'] ."/controllers/Roundbalancecontrol.php");
					// $in_group = $GLOBALS['bizconf']->getGroupId();
					// $server = Roundbalancecontrol::findServer($in_group);
					if (!is_int($server)) {
						require_once($GLOBALS['src_dir'] ."/controllers/Streamcontrol.php");
						$action = Streamcontrol::createStreamPath();
						if(!is_null($action)) {
							$return = array('result_code' => 200, 'result' => "http://".$server.$action);
							Logger::writelog(array('result_code' => 200, 'result' => "http://".$server.$action));
						}
						else $return = array('result_code' => 430, 'result' => "Cannot find playlist.");
					}
					// elseif ($server == "106") $return = array('result_code' => 200, 'result' => "406");
					else $return = array('result_code' => 420, 'result' => "Cannot find streaming server.");
				}
				else $return = array('result_code' => 410, 'result' => "Your concurrent reaches limit.");
			}
			else $return = array('result_code' => 610, 'result' => "Cannot find channel to streaming.");
		}
		else $return = array('result_code' => 600, 'result' => "Invalid request.");
		
		
		return $return;
    }
}
