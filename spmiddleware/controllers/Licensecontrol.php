<?
class Licensecontrol {
	
	## error code
	 # 200 license valid
	 # 441 invalid decrypt
	 # 442 expired license
	 # 443 invalid content
	
	public static function authenticate () {
		$secure_arr = json_decode(file_get_contents($GLOBALS['src_dir'] ."/assets/securestreamprovider/". $GLOBALS['json_object']->appid .".secure"), true);
		require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		// var_dump(urldecode($GLOBALS['json_object']->token));
		$decrypted_license = Opensslcryption::decrypt(urldecode($GLOBALS['json_object']->token), $secure_arr['key'], $secure_arr['vector']);
		// var_dump($decrypted_license);
		unset($secure_arr);
		if ($decrypted_license !== false) {
			$decrypted_license = explode("-", $decrypted_license);
			if (abs(time() - $decrypted_license[1]) <= 120) {
			// if (true) {
				if ( $decrypted_license[2] == $GLOBALS['json_object']->appid) {
					return 200;
				}
				else return 443;
			}
			else return 442;
		}
		else return 441;
	}
}