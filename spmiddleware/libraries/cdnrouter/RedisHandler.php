<?
class RedisHandler {
	const host = "172.22.162.236";
	const port = 6380;
	
	public static function setRedis ($key, $val) {
		$redis = new Redis();
		$redis->pconnect(self::host, self::port, 3, 'cdn_router_unit');
		$redis->set($key, $val);
		$redis->close();
	}
	
	public static function getRedis ($key) {
		$redis = new Redis();
		$redis->pconnect(self::host, self::port, 3, 'cdn_router_unit');
		$return = $redis->get($key);
		$redis->close();
		return $return;
	}
}