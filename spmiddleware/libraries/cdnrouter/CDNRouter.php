<?
require_once('PrefixLoader.php');
require_once('CDNZone.php');

// $loader = new PrefixLoader();
// $tree = $loader->loadPrefix(2, "IPPrefix.txt");
// RedisHandler::setRedis("PrefixLoader", json_encode($tree));
// $tree = json_decode(RedisHandler::getRedis("PrefixLoader"), true);
// $search = new PrefixSearch();
// $search->loadPrefixes($tree);
// var_dump($tree);

$start = microtime();
$tree = json_decode(RedisHandler::getRedis("PrefixLoader"), true);
$search = new PrefixSearch();
$search->loadPrefixes($tree);
$route = $search->searchThroughPrefix($argv[1]);
echo "CDN Zone id is ". $route."\n";
echo "route subnet time is ". (microtime() - $start) .".\n";

## correct prefix
// $start = microtime();
// $tree = json_decode(RedisHandler::getRedis("PrefixLoader"), true);
// $search = new PrefixSearch();
// $search->loadPrefixes($tree);
// $routes = $search->searchThroughPrefixWithChoose($argv[1]);
// rsort($routes);
// echo explode("::", $routes[0])[1]."\n";
// echo "route with find correct subnet time is ". abs(microtime() - $start) .".\n";

$cdnzone = new CDNZone();
// $cdnzone_arr = array("TYPE" => array("LIVE" => 1, "TIMESHIFT" => 2, "CATCHUP" =>2),"NEXT" => null);
// $cdnzone->setCDNZone(1, $cdnzone_arr);
// $cdnzone_arr = array("TYPE" => array("LIVE" => 3, "TIMESHIFT" => 4, "CATCHUP" =>4),"NEXT" => array(1));
// $cdnzone->setCDNZone(2, $cdnzone_arr);
echo "Edge group number is ".($cdnzone->getCDNZone($route, "TIMESHIFT"))."\n";