<?
require "../Mwredis.php";

$redis = new Mwredis();
## liveevent
// $conf = '{"application":"unlimit","user":"unlimit","visitor":["web","mobile"]}';
// $redis->setRedis("liveevent:config", $conf, 0);
// $redis->setRedis("liveevent:db", serialize(array("host" => "172.22.162.177", "port" => 6383)), 0);

// $conf = '{"application":"unlimit","user":"unlimit","visitor":["web","mobile"],"live":["218","021","058","064","080","082","086","107","108","109","110","111","114","127","135","139","143","148","151","152","154","159","179","207","c03","c05","c07","c09","c11","c12","d03","d05","d11","d13","d23","d33","d43","d48","d54","d56","d62","d76","d78","d81","d83","da0","da1","da6","da7","o009","o012","o013","o014","o015","o016","o017","o018","o019","o020","o021","o022","o023","o024","o025","o026","o027","o028","o029","o030","o031","o032","o033","o039","o042","o043","o044","o045","o053","o055","o057","o058","o059","o060","o062","o063","o065","o066","o068","o069","o070","o071","o064"],"timeshift":["021","058","064","080","082","086","107","108","109","110","111","114","127","135","139","148","151","152","154","159","179","207","c03","c05","c09","c11","c12","d13","d78","da0","da7","o009","o012","o014","o015","o016","o019","o020","o022","o024","o025","o030","o031"],"catchup":["021","135","207","159","058","139","107","127","064"]}';
// $redis->setRedis("htv:config", $conf, 0);
// $redis->setRedis("htv:db", serialize(array("host" => "172.22.162.177", "port" => 6381)), 0);

// $conf = '{"application":"unlimit","user":"unlimit","visitor":["web","mobile"],"live":["218","021","058","064","080","082","086","107","108","109","110","111","114","127","135","139","143","148","151","152","154","159","179","207","c03","c05","c07","c09","c11","c12","d03","d05","d11","d13","d23","d33","d43","d48","d54","d56","d62","d76","d78","d81","d83","da0","da1","da6","da7","o009","o012","o013","o014","o015","o016","o017","o018","o019","o020","o021","o022","o023","o024","o025","o026","o027","o028","o029","o030","o031","o032","o033","o039","o042","o043","o044","o045","o053","o055","o057","o058","o059","o060","o062","o063","o065","o066","o068","o069","o070","o071","o064"],"timeshift":["021","058","064","080","082","086","107","108","109","110","111","114","127","135","139","148","151","152","154","159","179","207","c03","c05","c09","c11","c12","d13","d78","da0","da7","o009","o012","o014","o015","o016","o019","o020","o022","o024","o025","o030","o031"],"catchup":["021","135","207","159","058","139","107","127","064"]}';
// $redis->setRedis("htvaec:config", $conf, 0);
// $redis->setRedis("htvaec:db", serialize(array("host" => "172.22.162.177", "port" => 6383)), 0);

// $conf = '{"application":"unlimit","user":"unlimit","visitor":["web","mobile"],"live":["218","021","058","064","080","082","086","107","108","109","110","111","114","127","135","139","143","148","151","152","154","159","179","207","c03","c05","c07","c09","c11","c12","d03","d05","d11","d13","d23","d33","d43","d48","d54","d56","d62","d76","d78","d81","d83","da0","da1","da6","da7","o009","o012","o013","o014","o015","o016","o017","o018","o019","o020","o021","o022","o023","o024","o025","o026","o027","o028","o029","o030","o031","o032","o033","o039","o042","o043","o044","o045","o053","o055","o057","o058","o059","o060","o062","o063","o065","o066","o068","o069","o070","o071","o064","ovp"],"timeshift":["021","058","064","080","082","086","107","108","109","110","111","114","127","135","139","148","151","152","154","159","179","207","c03","c05","c09","c11","c12","d13","d78","da0","da7","o009","o012","o014","o015","o016","o019","o020","o022","o024","o025","o030","o031"],"catchup":["021","135","207","159","058","139","107","127","064"]}';
// $redis->setRedis("tvtruelife:config", $conf, 0);
// $redis->setRedis("tvtruelife:db", serialize(array("host" => "172.22.162.177", "port" => 6382)), 0);

// $conf = '{"application":"unlimit","user":"unlimit","visitor":["stb","web","mobile"],"live":["135"],"timeshift":["135"],"catchup":["135"]}';
// $redis->setRedis("tnn:config", $conf, 0);
// $redis->setRedis("tnn:db", serialize(array("host" => "172.22.162.177", "port" => 6383)), 0);

// $conf = '{"application":"unlimit","user":"unlimit","visitor":["web","mobile"],"live":["207"],"timeshift":["207"],"catchup":["207"]}';
// $redis->setRedis("true4u:config", $conf, 0);
// $redis->setRedis("true4u:db", serialize(array("host" => "172.22.162.177", "port" => 6383)), 0);

// $conf = '{"application":"unlimit","user":"unlimit","visitor":["web","mobile"],"live":["139","107"],"timeshift":["139","107"],"catchup":["139","107"]}';
// $redis->setRedis("trueplook:config", $conf, 0);
// $redis->setRedis("trueplook:db", serialize(array("host" => "172.22.162.177", "port" => 6383)), 0);

// $conf = '{"application":"unlimit","user":"unlimit","visitor":["web","mobile"],"live":["139","107"],"timeshift":["139","107"],"catchup":["139","107"]}';
// $redis->setRedis("webno1:config", $conf, 0);
// $redis->setRedis("webno1:db", serialize(array("host" => "172.22.162.177", "port" => 6383)), 0);

// $conf = '{"application":"unlimit","user":"unlimit","visitor":["web","mobile"],"live":["d48","d54","d03","da6","d11","d13","d23","154","d78","d83","d76","d81","153","d33","d43","d56","d05","da0","da7","da1","o012","143","o013","o014","o015","o016","o017","o018","o019","o020","o021","o022","o023","o024","o025","151","o026","o027","o029","o030","o031","o032"],"timeshift":["135","d13","154","d78","da0","139","128","127","107","159","197","152","082","109","042","114","110","151","171","111","125","124","086","148","074","027","o002","d75","d20","ca05","211","ca03","o012","o030","o024","o022","o019","o015","o020","o016","o025","o029","o028","o034","o014","o037","o039"],"catchup":["135","d13","154","d78","139","128","127","107","159","197","109","042","114","110","171","111","125","124","086","148","074","ca05","211","ca03","o030","o024","o022","o019","o015","o020","o016","o025","o029","o028","o034","o014","o037","o039"]}';
// $redis->setRedis("anywhere:config", $conf, 0);
// $redis->setRedis("anywhere:db", 6383, 0);

// $conf = '{"application":"unlimit","user":"unlimit","visitor":["web","mobile"],"live":["127","218"],"timeshift":["127"],"catchup":["127"]}';
// $redis->setRedis("trueselect:config", $conf, 0);
// $redis->setRedis("trueselect:db", serialize(array("host" => "172.22.162.177", "port" => 6383)), 0);

$conf = '{"application":"unlimit","user":"1","visitor":["web","mobile"],"live":["013","021","058","064","075","080","082","086","105","107","108","109","110","111","114","123","127","135","139","143","148","151","152","154","159","179","184","207","c03","c05","c07","c09","c11","c12","d03","d05","d11","d13","d23","d33","d43","d48","d54","d56","d62","d76","d78","d81","d83","da0","da1","da6","da7","o009","o012","o013","o014","o015","o016","o017","o018","o019","o020","o021","o022","o023","o024","o025","o026","o027","o028","o029","o030","o031","o032","o033","o039","o042","o043","o044","o045","o053","o055","o057","o058","o059","o060","o062","o063","o065","o066","o068","o069","o070","o071","o064","202","215","216","217","219","ovp"],"timeshift":["021","058","064","080","082","086","107","108","109","110","111","114","127","202","215","216","217","219"],"catchup":["013","075","105","123","135","184","207","159","058","139","107","127","064","202","215","216","217","219"]}';
$redis->setRedis("trueid:config", $conf, 0, null, array("host" => "172.22.162.235", "port" => 6380));
// $redis->setRedis("trueid:db", serialize(array("host" => "172.22.162.235", "port" => 6381)), 0);

// $conf = '{"application":"unlimit","user":"unlimit","visitor":["web","mobile"],"live":["218","c03","c05","c07","c09","c11","c12","135","139","058","159","127","107","086","111","o031","o030","021"],"timeshift":["c03","c05","c07","c09","c11","c12","135","139","058","159","127","107","086","111","o031","o030","021"],"catchup":["c03","c05","c07","c09","c11","c12","135","139","058","159","127","107","086","111","o031","o030","021"]}';
// $redis->setRedis("hsport:config", $conf, 0);
// $redis->setRedis("hsport:db", 6383, 0);

// $conf = '{"application":"unlimit","user":"unlimit","visitor":["web"],"live":["r01cam1","r01cam2","r01cam3","r02cam1","r02cam2","r02cam3","r03cam1","r03cam2","r03cam3","r04cam1","r04cam2","r04cam3","r05cam1","r05cam2","r05cam3","r06cam1","r06cam2","r06cam3","r07cam1","r07cam2","r07cam3","r08cam1","r08cam2","r08cam3","r09cam1","r09cam2","r09cam3","r10cam1","r10cam2","r10cam3","r11cam1","r11cam2","r11cam3","r12cam1","r12cam2","r12cam3","r13cam1","r13cam2","r13cam3","r14cam1","r14cam2","r14cam3","r15cam1","r15cam2"]}';
// $redis->setRedis("pipr:config", $conf, 0);
// $redis->setRedis("pipr:db", 6383, 0);

// $conf = '{"application":"unlimit","user":"unlimit","visitor":["web","mobile"],"live":["ovp"],"timeshift":["ovp"],"catchup":["ovp"]}';
// $redis->setRedis("tvtlovp:config", $conf, 0);
// $redis->setRedis("tvtlovp:db", 6382, 0);

// $conf = '{"application":"unlimit","user":"unlimit","visitor":["web","mobile"],"live":["ovp"],"timeshift":["ovp"],"catchup":["ovp"]}';
// $redis->setRedis("trueidovp:config", $conf, 0);
// $redis->setRedis("trueidovp:db", serialize(array("host" => "172.22.162.235", "port" => 6381)), 0);

// $redis->setRedis("trueidtest:config", $conf, 0);
// $redis->setRedis("trueid:config", $conf, 0);
// $redis->setRedis("test:db", serialize(array("host" => "172.22.162.177", "port" => 6383)), 0);
echo "set";