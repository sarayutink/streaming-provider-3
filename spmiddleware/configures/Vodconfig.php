<?
class Vodconfig {
	var $bizunit;
    var $contentGroup, $redis, $device, $isCCUOver;
	
	public function __construct () {
		$this->bizconf = $this->loadBizConf($GLOBALS['json_object']->appid.".conf", $GLOBALS['src_dir'] ."/assets/". strtolower($GLOBALS['ctrl_name']) ."/");
	}
	
	private function loadBizConf ($appname, $path = APPPATH) {
		if (file_exists($path.$appname)) {
			/** load config in XML */
			try {
				return simplexml_load_file($path.$appname);
			}
			catch(Exception $e) {
				return false;
			}
		}
		else {
			return false;
		}
	}

	public function isBlacklist () {
		$host = "sprdhost";
        $port = 6380;
		$db = 7;
		$return = false;
        try {
			$redis = new Redis();
            $redis->connect($host, $port);
            $redis->select($db);
			$isBlacklist = $redis->exists($GLOBALS['json_object']->uid);
			$redis->close();
			if ($isBlacklist == 0) $return = false;
			else $return = true;
        } catch (Exception $e) {
            $return = false;
		}

        return $return;
    }

    public function isCCuOverGroup () {
		$ssoid = $GLOBALS['json_object']->uid;
		$device = $GLOBALS['json_object']->sessionid;
		$content = $GLOBALS['json_object']->channelid;
        $this->isCCUOver = false;
        $this->device = $device;
        // if (BZENABLE) {
        if (true) {
            try {
                $this->redis = new Redis;
                $this->redis->connect("10.18.19.102", 6379);
                # check whitelist
                $this->redis->select(2);
                $white = $this->redis->exists($ssoid);
                if ($white == 0) {
                    # get current content group
                    $this->redis->select(5);
                    // echo $content;
                    $this->contentGroup = $this->redis->get($content);
                    if (!is_null($this->contentGroup)) {
                        # get all active devices
                        $this->redis->select(3);
                        // echo $ssoid."|*|".$this->contentGroup;
                        $alldevices = $this->redis->keys($ssoid."|*|".$this->contentGroup);
                        // var_dump($alldevices);
                        if (count($alldevices) > 0) {
                            $this->isCCUOver = true;
                            array_walk($alldevices, array($this,'mapUnderGroup'));
                        }
                    }
                    $this->redis->close();
                }
                else {
                    $this->redis->close();
                    $this->isCCUOver = false;
                }
            }
            catch (Exception $e) {
                echo $e->getMessage() ."\n";
            }
        }
        return $this->isCCUOver;
    }

    private function mapUnderGroup ($device) {
        $devicesegment = explode("|", $device);
        $this->redis->select(5);
        $ctnGrp = $this->redis->get($devicesegment[3]);
        if ($this->contentGroup === $ctnGrp) {
            // var_dump(is_int(strpos($device, "|".$this->device."|")));
            if (is_int(strpos($device, "|".$this->device."|"))) $this->isCCUOver = false;
        }
    }
	
	public function verifyApplicationCCU ($ccucount = false) {
		if ($this->bizconf->application->ccu == 'unlimit') return true;
		else return $ccucount < $this->bizconf->application->ccu;
	}
	
	public function verifyUserCCU ($ccucount = false) {
		if ($this->bizconf->user->ccu == 'unlimit') return true;
		else return $ccucount < $this->bizconf->user->ccu;
	}
	
	public function generatePlaylist () {
		return "/". $this->bizconf->streammapping->AppInst ."/". $GLOBALS['json_object']->streamname. "/playlits.m3u8";
	}
	
	public function getBalencer () {
		return "http://server_loadbalan:8080/lalaynya.php";
	}
	
	public function generateManifest () {
		switch (@$GLOBALS['json_object']->drm) {
			case "wv":
				require_once $GLOBALS['src_dir'] ."/configures/ezdrm/Wvconfig.php";
				return Wvconfig::generateManifest();
			break;
			case "fp":
				require_once $GLOBALS['src_dir'] ."/configures/ezdrm/Fpconfig.php";
				return Fpconfig::generateManifest();
			break;
			default:
				require_once $GLOBALS['src_dir'] ."/configures/ezdrm/Aesconfig.php";
				return Aesconfig::generateManifest();
			break;
		}
	}
	
	public function getGroupId () {
		$drm = (@$GLOBALS['json_object']->drm == "wv" || @$GLOBALS['json_object']->drm == "fp") ? $GLOBALS['json_object']->drm : "aes";
		return $this->bizconf->$drm->group;
		// $group_arr = array('sportclip_truetv', 'sportclip_trueidv2', 'skyclipv2');
		// return (in_array($GLOBALS['json_object']->appid, $group_arr)) ? 103 : 105;
	}
	
	public function changeDateFormat ($dateformat, $fromat) {
		return date($fromat, $dateformat);
	}
	
	public function getrsaqstring () {
		require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		$str2decrypt = time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|". $GLOBALS['json_object']->csip ."|". explode("_", $GLOBALS['json_object']->streamname)[0] ."|". $GLOBALS['json_object']->uid ."|unlimit";
		$encrypt = Opensslcryption::encrypt($str2decrypt);
		// $encrypt = Opensslcryption::encryptbypass();
		$querystring = "appid={$GLOBALS['json_object']->appid}&uid={$GLOBALS['json_object']->uid}&sessionid={$GLOBALS['json_object']->sessionid}&mpass={$encrypt}&visitor={$GLOBALS['json_object']->visitor}";
		
		return $querystring;
	}
	
	public function getLicense () {
		switch (@$GLOBALS['json_object']->drm) {
			case "wv":
				require_once $GLOBALS['src_dir'] ."/configures/ezdrm/Wvconfig.php";
				return Wvconfig::generateLicense();
			break;
			case "fp":
				require_once $GLOBALS['src_dir'] ."/configures/ezdrm/Fpconfig.php";
				return Fpconfig::generateLicense();
			break;
			default:
				require_once $GLOBALS['src_dir'] ."/configures/ezdrm/Aesconfig.php";
				return Aesconfig::generateLicense();
			break;
		}
	}
}