<?
class Streamconfig2 {
	var $bizctrl;
	var $bizconf;
	var $confdb = 0;
	var $chdb = 1;
	
	public function __construct () {
		$this->bizctrl = $this->loadRedisXML();
		$this->bizconf = $this->loadRedisConfig();
	}
	
	private function loadRedisXML () {
		$xml_str = "<?xml version='1.0' encoding='UTF-8'?><configure>";
		$xml_str .= '<streammapping>'. $GLOBALS['redis']->getRedis($GLOBALS['json_object']->channelid, 1) .'</streammapping>';
		$xml_str .= '</configure>';
		
		return simplexml_load_string($xml_str);
	}
	
	private function loadRedisConfig () {
		$json_str = $GLOBALS['redis']->getRedis($GLOBALS['json_object']->appid .":config", 0);
		
		return json_decode($json_str);
	}
	
	private function getAppDB () {
		$serial = $GLOBALS['redis']->getRedis($GLOBALS['json_object']->appid .":db", 0);
		
		return unserialize($serial);
	}
	
	private function loadLocalXML ($channel, $filepath = "/www/webapps/providers_v3/spmiddleware/assets/streamingproviderv2/channels/") {
		if (file_exists($filepath.$channel.".channel")) {
			try {
				$xml_str = "<?xml version='1.0' encoding='UTF-8'?><configure>";
				$xml_str .= '<streammapping>'. file_get_contents($filepath.$GLOBALS['json_object']->channelid.".channel") .'</streammapping>';
				$xml_str .= '</configure>';
				
				return simplexml_load_string($xml_str); 
			}
			catch(Exception $e) {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	public function isValidChannelID () {
		return true;
	}
	
	public function generatePlaylist () {
		$visitor = "mobile";
		$query = '//streamprofile/language[@id="'. strtolower($GLOBALS['json_object']->langid) .'"]/profile[@streamlevel="'. strtolower($GLOBALS['json_object']->streamlvl) .'"  and @type="'. strtolower($GLOBALS['json_object']->type) .'"]';
		$ret_arr = $this->bizctrl->xpath($query);
		$appinst = (string)$ret_arr[0]->appinst;
		if (count($ret_arr) > 0) {
			require_once $GLOBALS['src_dir'] .'/configures/Blackoutconfig.php';
			$smil = str_replace("yyyymmdd", date("Ymd", (int)@$GLOBALS['json_object']->stime), (string)$ret_arr[0]->smil[0]);
			if ($GLOBALS['json_object']->type == "live" && Blackoutconfig::isBlackout($GLOBALS['json_object']->appid, $GLOBALS['json_object']->channelid)) {
				$smil = "bk_m_auto.smil";
				$appinst = "liveedge_app";
			}
			## chromecast smil
			$smil = $GLOBALS['json_object']->visitor != "chromecast" ? $smil : str_replace("_m_", "_tv_", $smil);
			$GLOBALS['signature'] = $this->signQueryString($smil);
			return "/".$appinst."/". $smil ."/playlist.m3u8";
		}
		else return null;
	}
	
	public function generateManifest () {
		switch ($GLOBALS['json_object']->drm) {
			case "wv":
				require_once $GLOBALS['src_dir'] ."/configures/ezdrm/Wvconfig.php";
				return Wvconfig::generateManifest();
			break;
			case "fp":
				require_once $GLOBALS['src_dir'] ."/configures/ezdrm/Fpconfig.php";
				return Fpconfig::generateManifest();
			break;
			default:
				return null;
			break;
		}
	}
	
	public function getGroupId () {
		$conf_path = "/www/webapps/providers_v3_static_group.xml";
		$group_conf = simplexml_load_file($conf_path);
		
		$group = ($GLOBALS['json_object']->channelid <> "ev01" ? $group_conf->{$GLOBALS['json_object']->appid}->{$GLOBALS['json_object']->type} : 6);
		if (is_null($group)) $group = $group_conf->base->{$GLOBALS['json_object']->type};
		
		return (string)$group;
	}
	
	public function getBalencer () {
		return "http://server_loadbalan:8080/lalaynya.php";
	}
	
	public function changeDateFormat ($dateformat, $fromat) {
		return date($fromat, $dateformat);
	}
	
	public function getrsaqstring () {
		require_once $GLOBALS['src_dir'] .'/configures/Blackoutconfig.php';
		require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		$channelid = ($GLOBALS['json_object']->channelid != '021') ? $GLOBALS['json_object']->channelid : '135';
		$channelid = ($GLOBALS['json_object']->channelid != 'en107') ? $GLOBALS['json_object']->channelid : '107';
		$dvr = ($GLOBALS['json_object']->type == "live") ? "live" : "dvr";
		$stream = !Blackoutconfig::isBlackout($GLOBALS['json_object']->appid, $GLOBALS['json_object']->channelid) ? $channelid : "bk";
		$encrypt = Opensslcryption::encrypt(time() ."|". $GLOBALS['json_object']->sessionid."_".$this->generateRandomString() ."|". $GLOBALS['json_object']->appid ."|". $GLOBALS['json_object']->csip ."|". $stream ."|". $GLOBALS['json_object']->uid ."|". $dvr);
		
		if ($GLOBALS['json_object']->type == "timeshift") @$querystring = "dvr=&";
		elseif ($GLOBALS['json_object']->type == "catchup") @$querystring = "dvr=&wowzadvrplayliststart={$this->changeDateFormat($GLOBALS['json_object']->stime, "YmdHis")}&wowzadvrplaylistduration={$GLOBALS['json_object']->duration}&";
		@$querystring .= "appid={$GLOBALS['json_object']->appid}&visitor={$GLOBALS['json_object']->visitor}&uid={$GLOBALS['json_object']->uid}&mpass={$encrypt}". $GLOBALS['signature'];
		
		return $querystring;
	}
	
	public function getLicense () {
		switch ($GLOBALS['json_object']->drm) {
			case "wv":
				require_once $GLOBALS['src_dir'] ."/configures/ezdrm/Wvconfig.php";
				return Wvconfig::generateLicense();
			break;
			case "fp":
				require_once $GLOBALS['src_dir'] ."/configures/ezdrm/Fpconfig.php";
				return Fpconfig::generateLicense();
			break;
			default:
				return null;
			break;
		}
	}
	
	private function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	} 
	
	public function signQueryString ($smil) {
		$secret = "ǝɯƖ┴ǝɔ@dSln0SʎʇƖl@ǝɹɹǝʍ0ԀpuƖW";
        $sid = bin2hex(openssl_random_pseudo_bytes(5));
		$rt = "".time();
	
		$payload = array($sid => array(
			$GLOBALS['json_object']->appid, 
			$smil,
			$GLOBALS['json_object']->type,
			$GLOBALS['json_object']->visitor,
			$GLOBALS['json_object']->uid,
			str_replace(array('+', '/', '='), array('-', '_' , ''), base64_encode($GLOBALS['json_object']->sessionid)),
			$rt	
		));
		
		$payloadKey = hash_hmac("sha256", $secret, $sid);
		
		$token = str_replace(array("+", "/", "="), array("-", "_", ""), base64_encode(hash_hmac("sha256", json_encode($payload), $payloadKey, true)));
	
		return "&sid={$sid}&rt={$rt}&tk={$token}";
	}
}