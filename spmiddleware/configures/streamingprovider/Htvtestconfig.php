<?
require_once($GLOBALS['src_dir'] ."/configures/Streamconfig.php");
class Htvtestconfig extends Streamconfig {	
	public function __construct () {
		parent::__construct();
	}
	
	public function getGroupId () {
		$conf_path = "/www/webapps/providers_v3_static_group.xml";
		$group_conf = simplexml_load_file($conf_path);
		$group = $group_conf->{$GLOBALS['json_object']->appid}->{$GLOBALS['json_object']->visitor};
		if (!is_null($group)) return $group;
		else return parent::getGroupId();
	}
}
?>