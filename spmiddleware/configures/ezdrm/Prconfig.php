<?
class Prconfig {
	public static function generateManifest () {
		$streamname = ($GLOBALS['json_object']->visitor == "stb") ? "_stb_auto.smil" : "_m_auto.smil";
		$streamname = str_pad($GLOBALS['json_object']->streamname, 12, "0", STR_PAD_LEFT) . $streamname;
		return isset($GLOBALS['bizconf']->bizconf->streammapping->appinst) ? "/". $GLOBALS['bizconf']->bizconf->streammapping->appinst ."/". $streamname ."/manifest.mpd?appid=". $GLOBALS['json_object']->appid ."&uid=". $GLOBALS['json_object']->uid ."&visitor=". $GLOBALS['json_object']->visitor : null;
	}
	
	public static function generateLicense () {
		require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		$str2decrypt = time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|". $GLOBALS['json_object']->csip ."|". explode("_", $GLOBALS['json_object']->streamname)[0] ."|". $GLOBALS['json_object']->uid ."|unlimit";
		$encrypt = Opensslcryption::encrypt($str2decrypt);
		$querystring = isset($GLOBALS['bizconf']->bizconf->drm->{$GLOBALS['json_object']->drm}) ? "pX=". $GLOBALS['bizconf']->bizconf->drm->{$GLOBALS['json_object']->drm} ."&appid={$GLOBALS['json_object']->appid}&uid={$GLOBALS['json_object']->uid}&sessionid={$GLOBALS['json_object']->sessionid}&mpass={$encrypt}": null;
		
		return $querystring;
	}
	
	public static function generateBypassLicense () {
		$encrypt = Opensslcryption::encryptbypass();
		$querystring = "pX=868497&appid={$GLOBALS['json_object']->appid}&uid={$GLOBALS['json_object']->uid}&mpass={$encrypt}";
		
		return $querystring;
	}
}