<?
class Wvconfig {
	public static function generateManifest () {
		switch ($GLOBALS['ctrl_name']) {
			case 'Streamingprovider2' :
				$manifest = self::generateLiveSmil();
			break;
			default :
				$manifest = self::generateVodSmil();
			break;
		}
		return $manifest;
	}
	
	public static function generateLicense () {
		switch ($GLOBALS['ctrl_name']) {
			case 'Streamingprovider2' :
				$qstring = self::generateLiveQString();
			break;
			default :
				$qstring = self::generateVodQString();
			break;
		}
		return $qstring;
	}
	
	public static function generateByPassLicense () {
		require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		$encrypt = Opensslcryption::encryptbypass();
		isset($GLOBALS['signature']) ?: $GLOBALS['signature'] = self::signQueryString();
		$querystring = "appid={$GLOBALS['json_object']->appid}&uid={$GLOBALS['json_object']->uid}&mpass={$encrypt}".$GLOBALS['signature'];
		
		return $querystring;
	}
	
	private static function generateLiveSmil () {
		$visitor = "mobile";
		$query = '//streamprofile/language[@id="'. strtolower($GLOBALS['json_object']->langid) .'"]/profile[@streamlevel="'. strtolower($GLOBALS['json_object']->streamlvl) .'"  and @type="'. strtolower($GLOBALS['json_object']->type) .'"]';
		$ret_arr = $GLOBALS['bizconf']->bizctrl->xpath($query);
		if (count($ret_arr) > 0) {
			require_once $GLOBALS['src_dir'] .'/configures/Blackoutconfig.php';
			// var_dump($object);
			$smil = str_replace("yyyymmdd", date("Ymd", (int)$GLOBALS['json_object']->stime), (string)$ret_arr[0]->smil[0]);
			if ($GLOBALS['json_object']->type == "live" && Blackoutconfig::isBlackout($GLOBALS['json_object']->appid, $GLOBALS['json_object']->channelid)) $smil = "bk_m_auto.smil";
			
			$GLOBALS['smil'] = $smil;
			$GLOBALS['signature'] = self::signQueryString();
			return isset($GLOBALS['bizconf']->bizconf->wv->appinst) ? "/". $GLOBALS['bizconf']->bizconf->wv->appinst ."/". $smil ."/manifest.mpd?appid=". $GLOBALS['json_object']->appid ."&uid=". $GLOBALS['json_object']->uid ."&visitor=". $GLOBALS['json_object']->visitor.$GLOBALS['signature'] : null;
		}
		else return null;
	}
	
	private static function generateVodSmil () {
		$streamname = "";
		// if (is_numeric($GLOBALS['json_object']->streamname)) {
		if (strpos($GLOBALS['json_object']->appid, "clip") === false) {
			// $streamname = ($GLOBALS['json_object']->visitor == "stb") ? "_stb_auto.smil" : "_m_auto.smil";
			$streamname = "_m_auto.smil";
			if (is_numeric($GLOBALS['json_object']->streamname)) $streamname = str_pad($GLOBALS['json_object']->streamname, 12, "0", STR_PAD_LEFT) . $streamname;
			else $streamname = $GLOBALS['json_object']->streamname . $streamname;
		}
		else {
			$streamname = $GLOBALS['json_object']->streamname;
		}
		$GLOBALS['smil'] = $streamname;
		$GLOBALS['signature'] = self::signQueryString();
		
		return isset($GLOBALS['bizconf']->bizconf->wv->appinst) ? "/". $GLOBALS['bizconf']->bizconf->wv->appinst ."/". $streamname ."/manifest.mpd?appid=". $GLOBALS['json_object']->appid ."&uid=". $GLOBALS['json_object']->uid ."&visitor=". $GLOBALS['json_object']->visitor.$GLOBALS['signature'] : null;
	}
	
	private static function generateLiveQString () {
		require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		$str2decrypt = time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|". $GLOBALS['json_object']->csip ."|". $GLOBALS['json_object']->channelid ."|". $GLOBALS['json_object']->uid ."|unlimit";
		$encrypt = Opensslcryption::encrypt($str2decrypt);
		$querystring = isset($GLOBALS['bizconf']->bizconf->drm->{$GLOBALS['json_object']->drm}) ? "appid={$GLOBALS['json_object']->appid}&uid={$GLOBALS['json_object']->uid}&sessionid={$GLOBALS['json_object']->sessionid}&mpass={$encrypt}".$GLOBALS['signature'] : null;
		
		return $querystring;
	}
	
	private static function generateVodQString () {
		require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		$str2decrypt = time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|". $GLOBALS['json_object']->csip ."|". explode("_", $GLOBALS['json_object']->streamname)[0] ."|". $GLOBALS['json_object']->uid ."|unlimit";
		$encrypt = Opensslcryption::encrypt($str2decrypt);
		$querystring = isset($GLOBALS['bizconf']->bizconf->drm->{$GLOBALS['json_object']->drm}) ? "appid={$GLOBALS['json_object']->appid}&uid={$GLOBALS['json_object']->uid}&sessionid={$GLOBALS['json_object']->sessionid}&mpass={$encrypt}".$GLOBALS['signature'] : null;
		
		return $querystring;
	}
	
	private static function signQueryString ($smil) {
		$secret = "ǝɯƖ┴ǝɔ@dSln0SʎʇƖl@ǝɹɹǝʍ0ԀpuƖW";
        $sid = bin2hex(openssl_random_pseudo_bytes(5));
		$rt = "".time();
	
		$payload = array($sid => array(
			$GLOBALS['json_object']->appid, 
			$smil,
			$GLOBALS['json_object']->type,
			$GLOBALS['json_object']->visitor,
			$GLOBALS['json_object']->uid,
			str_replace(array('+', '/', '='), array('-', '_' , ''), base64_encode($GLOBALS['json_object']->sessionid)),
			$rt	
		));
		
		$payloadKey = hash_hmac("sha256", $secret, $sid);
		
		$token = str_replace(array("+", "/", "="), array("-", "_", ""), base64_encode(hash_hmac("sha256", json_encode($payload), $payloadKey, true)));
	
		return "&sid={$sid}&rt={$rt}&tk={$token}";
	}
}