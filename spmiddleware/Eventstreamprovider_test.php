<?
require_once 'Eventstreamprovider.req';

class Eventstreamprovider {
	public static function provide ($request, $response) {
		$body = $request->getBody();
		$GLOBALS['ctrl_name'] = "Eventstreamprovider";
		$GLOBALS['json_object'] = json_decode($body);
		unset($body);
		
		## validate post json
		if (Variable::validate()) {
			$apconf = ucfirst($GLOBALS['json_object']->appid)."config";
			require_once($GLOBALS['src_dir'] ."/configures/eventstreamprovider/".$apconf.".php");
			var_dump($GLOBALS['src_dir'] ."/configures/eventstreamprovider/".$apconf.".php");
			$GLOBALS['bizconf'] = new $apconf();
			// ## check ccu control
			// require_once($GLOBALS['src_dir'] ."/controllers/Ccucontrol.php");
			// if (Ccucontrol::check()) {
			// var_dump($GLOBALS);
		}
		else $return = array('result_code' => 600, 'result' => "Invalid request.");
		
		
		return $return;
    }
}